# Remote Health Care Monitoring System #

This repository contains the web application for the IoT project RMHS to add patient personas and manage their details.

### Where is the client server apps? ###

The client server app which make use of the data in the personas and communicate with the Raspberry to help the patient is stored in a different repository and made private because of some copyright concerns by the School.