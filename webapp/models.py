from __future__ import unicode_literals

from django.db import models

class patient(models.Model):

   name = models.CharField(max_length = 50)
   age = models.IntegerField()
   address = models.TextField()
   phonenumber = models.IntegerField()
   GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
   gender = models.CharField(max_length=1,choices = GENDER_CHOICES)
   temp = models.IntegerField()
   pulse = models.IntegerField()
   history = models.TextField(null = True)
   notes = models.ForeignKey('prescription', default = 1)
   status_choices = (
   		('N', 'Normal'),
   		('C', 'Critical'),
    )
   status = models.CharField(max_length=1, choices = status_choices)

   class Meta:
      db_table = "My Patient"

class prescription(models.Model):
	comment = models.TextField()
	prescription = models.TextField(null = True)
	class Meta:
		db_table = "Prescription"