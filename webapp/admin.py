from django.contrib import admin

from models import patient,prescription

admin.site.register(patient)
admin.site.register(prescription)
# Register your models here.
